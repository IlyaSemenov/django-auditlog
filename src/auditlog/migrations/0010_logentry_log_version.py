# Generated by Django 2.0.8 on 2018-11-29 09:29

from django.db import migrations, models


def set_default_str(apps, schema_editor):
    LogEntry = apps.get_model('auditlog.LogEntry')
    LogEntry.objects.update(log_version=0)


class Migration(migrations.Migration):

    dependencies = [
        ('auditlog', '0009_change_additional_data'),
    ]

    operations = [
        migrations.AddField(
            model_name='logentry',
            name='log_version',
            field=models.IntegerField(choices=[(0, 'str'), (1, 'json')], default=1),
        ),
        migrations.RunPython(set_default_str, migrations.RunPython.noop),
    ]
